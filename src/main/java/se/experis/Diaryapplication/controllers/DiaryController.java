package se.experis.Diaryapplication.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.Diaryapplication.models.CommonResponse;
import se.experis.Diaryapplication.models.Diary;
import se.experis.Diaryapplication.repositories.DiaryRepository;
import se.experis.Diaryapplication.utils.Command;
import se.experis.Diaryapplication.utils.logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController
public class DiaryController {

    @Autowired
    DiaryRepository diaryRepository;

    @GetMapping("/diary/{id}")
    public ResponseEntity<CommonResponse> getAuthorById(HttpServletRequest request, @PathVariable("id") Integer id){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(diaryRepository.existsById(id)) {
            cr.data = diaryRepository.findById(id);
            cr.message = "Author with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Author not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/diary/all")
    public ResponseEntity<CommonResponse> getAllAuthors(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = diaryRepository.findAll();
        cr.message = "All authors";

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/diary")
    public ResponseEntity<CommonResponse> authorRoot(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        cr.message = "Not implemented";

        HttpStatus resp = HttpStatus.NOT_IMPLEMENTED;

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PostMapping("/diary")
    public ResponseEntity<CommonResponse> addAuthor(HttpServletRequest request, HttpServletResponse response, @RequestBody Diary diary){

        Command cmd = new Command(request);
        //process
        diary = diaryRepository.save(diary);

        CommonResponse cr = new CommonResponse();
        cr.data = diary;
        cr.message = "New author with id: " + diary.id;

        HttpStatus resp = HttpStatus.CREATED;

        response.addHeader("Location", "/diary/" + diary.id);

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PutMapping("/diary/{id}")
    public ResponseEntity<CommonResponse> updateAuthor(HttpServletRequest request, @RequestBody Diary editDiary, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(diaryRepository.existsById(id)) {
            Optional<Diary> authorRepo = diaryRepository.findById(id);
            Diary diary = authorRepo.get();

            if(editDiary.title != null) {
                diary.title = editDiary.title;
            }
            if(editDiary.content != null) {
                diary.content= editDiary.content;
            }
            if(editDiary.URL != null) {
                diary.URL = editDiary.URL;
            }
            if(editDiary.date != null) {
                diary.date = editDiary.date;
            }

            diaryRepository.save(diary);

            cr.data = diary;
            cr.message = "Updated author with id: " + diary.id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Author not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }


}
