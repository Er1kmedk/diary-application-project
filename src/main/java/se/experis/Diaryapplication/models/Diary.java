package se.experis.Diaryapplication.models;

import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;
import java.util.Date;


@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class Diary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String title;

    @Column(length = 15099)
    public String content;

    @Column(length = 15099)
    public String URL;

    @Column
    public Date date;

    public Diary() {
        Date date = new Date();
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public Date getDate() {
        return date;
    }

}
