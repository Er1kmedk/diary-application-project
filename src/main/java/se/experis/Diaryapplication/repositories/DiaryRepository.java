package se.experis.Diaryapplication.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.Diaryapplication.models.Diary;

public interface DiaryRepository extends JpaRepository<Diary, Integer> {
    Diary getById(Integer id);
}
