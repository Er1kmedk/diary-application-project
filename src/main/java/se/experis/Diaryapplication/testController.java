package se.experis.Diaryapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.Diaryapplication.models.Diary;
import se.experis.Diaryapplication.repositories.DiaryRepository;

import java.util.List;
import java.util.Optional;


@Controller
public class testController {

    @Autowired
    DiaryRepository diaryRepository;

    @GetMapping("/listposts")
    public String getWeb(Model model){
        List<Diary> allDiarys = diaryRepository.findAll();
        model.addAttribute("alldiarys",allDiarys);
        return "listposts";
    }

    @GetMapping("/editpost/{id}")
    public String editPost(@PathVariable Integer id, Model model) {
        if (diaryRepository.existsById(id)) {
            Optional<Diary> diaryToBeEdited = diaryRepository.findById(id);
            Diary diary = diaryToBeEdited.get();
            model.addAttribute("editDiary", diary);
        }
        return "editpost.html";
    }

    @GetMapping("/deletepost/{id}")
    public String deletePost(@PathVariable Integer id) {
        if (diaryRepository.existsById(id)) {
            diaryRepository.deleteById(id);
        }
        return "redirect:/listposts";
    }

    @GetMapping("/newpost")
    public String newPost(Model model) {
        model.addAttribute("diary", new Diary());
        return "newpost.html";
    }


    @PostMapping("/redirect")
    public String redirect(@ModelAttribute("blogpost") Diary diary) {
        diaryRepository.save(diary);
        return "redirect:/listposts";
    }
}
